<?php
use Illuminate\Support\Collection;
use Illuminate\View\View;
class NavigationViewComposer {
	public function compose(View $view){
		$menu = New Collection;
		$i = 0;
		while($i<5){
			$j = rand(1,5);
			$k = 0;
			while($k<$j){
				$submenu = New Collection;
				$submenu->push((object)[
					'title'=>$k.str_random(rand(1,10))
					,'link'=>URL::to(str_random(rand(1,5)))
				]);
				$k++;
			}
			$menu->push((object)[
				'title'=>$i.str_random(rand(1,10))
				,'menu'=>$submenu
			]);
			$i++;
		}
		$view->menu = $menu;
	}
}
<nav>
	<div class="row nav">
		<ul class="nav nav-pills">
			@foreach($menu->all() as $item)
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					{{$item->title}} <span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					@foreach($item->menu->all() as $_item)
						<li><a href="{{$_item->link}}">{{$_item->title}}</a></li>
					@endforeach
				</ul>
			</li>
			<li class="divider"><hr></li>
			@endforeach
		</ul>
	</div>
</nav>
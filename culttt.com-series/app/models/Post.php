<?php

class Post extends \Eloquent {
	protected $fillable = ['body'];
	public function user(){
		return $this->belongsTo('User');
	}
}
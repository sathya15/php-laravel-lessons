<?php
use Repositories\User\IUserRepository;
class UserController extends BaseController{
	public function __construct(IUserRepository $user){
		$this->user = $user;
	}
	public function getIndex(){
		$users = $this->user->getAllUsers();
		return View::make('user.index',compact('users'));
	}
	public function getUser($id){
		$user = $this->user->getUserById($id);
		return View::make('user.profile',compact('user'));
	}
	public function saveUser($id=null){
		if(is_null($id))
			$this->user->createOrUpdate($id);
		else
			$this->user->createOrUpdate();
	}
}
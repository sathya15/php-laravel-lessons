<?php
namespace Repositories\User;
class UserRepository implements IUserRepository {
	public function __construct(\User $user){
		$this->user = $user;
	}
	public function getAllUsers(){
		return \User::all();
	}
	public function getUserById($id){
		return \User::find($id);
	}
	public function createOrUpdate($id=null){
		if(is_null($id)){
			$user = new \User;
			//$user->name = 'Sheikh Heera';
			$user->email = 'me@yahoo.com';
			$user->password = '123456';
			return $user->save();
		}else{
			$user = \User::find($id);
			//$user->name = 'Sheikh Heera2';
			$user->email = 'me@yahoo.com';
			$user->password = '123456';
			return $user->save();
		}
	}
	//enable calling of User class methods
	public function __call($method,$args){
		return call_user_func_array([$this->user,$method], $args);
	}
}